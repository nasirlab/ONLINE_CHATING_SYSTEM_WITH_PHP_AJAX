<?php 
namespace App;
use PDO;
/**
* Database class
*/
class Chat extends PDO
{	
	private $dsn = 'mysql:host=localhost;dbname=chat';
	private $dbuser = 'root';
	private $dbpassword  = ''; 
	
	function __construct()
	{
		try{
			parent::__construct($this->dsn,$this->dbuser,$this->dbpassword);
		}catch(PDOExecption $e){
			echo 'Error'.$e->getMessage();
		}
	}

	public function index(){
		$sql = "SELECT * FROM chat_tbl ORDER BY id ASC";
		$stmt = $this->prepare($sql);
		$stmt->execute();
		$data = $stmt->fetchAll();
		return $data;
	}

	public function store($data=''){
		$sql = "INSERT INTO chat_tbl (name,message) VALUES (:name,:message)";
		$stmt = $this->prepare($sql);
		$insert = $stmt->execute(array(
				':name'=>$data['name'],
				':message'=>$data['message'],
			));
		return $insert;
	}
}