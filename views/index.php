<?php
	include_once('../vendor/autoload.php');
	$obj = new App\Chat;


 ?>
<!doctype html>
<html class="no-js" lang="">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="x-ua-compatible" content="ie=edge">
        <title></title>
        <meta name="description" content="">
        <meta name="viewport" content="width=device-width, initial-scale=1">

		<title>Online chating system</title>
		<link rel="stylesheet" type="text/css" href="../asset/css/bootstrap.css">
		<link rel="stylesheet" type="text/css" href="../asset/css/style.css">

        <script type="text/javascript">

            function ajax(){
                var req = new XMLHttpRequest();
                req.onreadystatechange = function(){
                    if(req.readyState == 4 && req.status == 200){
                        document.getElementById('chatid').innerHTML = req.responseText;
                    }
                }
                req.open('GET','chatdata.php',true);
                req.send();
            }
            setInterval(function(){ajax()},1000);
        </script>

	</head>
	<body onload="ajax()">
	    <!--[if lt IE 8]>
            <p class="browserupgrade">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> to improve your experience.</p>
        <![endif]-->
	<div class="container">
    <div class="row">
        <div class="col-md-5">
            <div class="panel panel-primary">
                <div class="panel-heading">
                    <span class="glyphicon glyphicon-comment"></span> Chat
                </div>
                <div class="panel-body">
                    <ul class="chat">
						
                        <div id="chatid" ></div>

                



<!--                         <li class="right clearfix"><span class="chat-img pull-right">
                            <img src="http://placehold.it/50/FA6F57/fff&text=ME" alt="User Avatar" class="img-circle" />
                        </span>
                            <div class="chat-body clearfix">
                                <div class="header">
                                    <small class=" text-muted"><span class="glyphicon glyphicon-time"></span>13 mins ago</small>
                                    <strong class="pull-right primary-font">Bhaumik Patel</strong>
                                </div>
                                <p>
                                    Lorem ipsum dolor sit amet, consectetur adipiscing elit. Curabitur bibendum ornare
                                    dolor, quis ullamcorper ligula sodales.
                                </p>
                            </div>
                        </li>
 -->

                    </ul>
                </div>
                <div class="panel-footer">


               	<form action="" method="POST"> 
                    <div class="input-group">
                        <input id="btn-input" type="text" name="name" class="form-control input-sm" placeholder="Type your name here..." />
                        <textarea name="message" class="form-control input-sm" ></textarea>
                        <span class="input-group-btn">
                            <button name="submit" class="btn btn-warning btn-sm" id="btn-chat">
                                Send</button>
                        </span>
                    </div>
                   </form> 
                   <?php  
                        if (isset($_POST['submit'])) {
                            $insert = $obj->store($_POST);
                            if ($insert == true) {
                               echo "<audio hidden='true' src='../asset/audio/beeptone.wav' autoplay='true' >";
                            }
                        }

                     ?>


                </div>
            </div>
        </div>
    </div>
</div>

		
	</body>
</html>